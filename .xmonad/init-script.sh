#!/bin/sh

# /etc/X11/xinit/xinitrc
#
# global xinitrc file, used by all X sessions started by xinit (startx)

# enable compton
compton -b

# Set mouse cursor
xsetroot -cursor_name left_ptr &

# set wallpaper
feh --bg-scale ~/Pictures/getcape.jpg

# enable horizontal scroll
synclient HorizTwoFingerScroll=1 &

# enable notification-daemon
#/usr/lib/notification-daemon/notification-daemon &

# enable screensaver
xautolock -time 60 -locker "gnome-screensaver-command -l" -notify 10 -notifier "notify-send -t 5000 -i gtk-dialog-info 'Locking in 10 seconds'" &

# enable dropbox daemon
dropbox start &

# invoke global X session script
#exec xmonad
